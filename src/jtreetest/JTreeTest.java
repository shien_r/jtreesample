package jtreetest;

import java.awt.Dimension;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class JTreeTest extends JFrame {
	  private static final long serialVersionUID = 1L;
	  
	JTreeTest () {
		super();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		FileSystemView fileSystemView = FileSystemView.getFileSystemView();
		DefaultMutableTreeNode root = new DefaultMutableTreeNode();
		DefaultTreeModel treeModel = new DefaultTreeModel(root);
		
		// ホームディレクトリをセットする。それ以下を読み込むよ
		File fileRoot = fileSystemView.getDefaultDirectory();
		
		// ルート直下のファイルの読み込みとノードに登録
		for (File fileSystemRoot: fileRoot.listFiles()) {
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileSystemRoot);
			root.add(node);
			for (File file: fileSystemView.getFiles(fileSystemRoot, true)) {
				if (file.isDirectory()) {
					node.add(new DefaultMutableTreeNode(file));
				}
			}
		}
		JTree tree = new JTree(treeModel);
		// createEmptyBorder:上下左右の辺の幅を指定して、スペースをとるが、描画を行わない空のボーダーを生成
		tree.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		tree.setRootVisible(false);
		// 一段下のファイルの処理を書く
		tree.addTreeWillExpandListener(new FolderWillExpandListener(fileSystemView));
		// 各ノードの見た目を設定するためのやつを登録
		tree.setCellRenderer(new FileTreeCellRenderer(tree.getCellRenderer(), fileSystemView));
		// expandRow:指定された行にあるノードが展開され、表示可能になるように 0行目を指定
		tree.expandRow(0);
		
	    this.add(new JScrollPane(tree));
	    this.setSize(new Dimension(200,500));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JTreeTest jt = new JTreeTest();
		jt.setVisible(true);
	}
}
